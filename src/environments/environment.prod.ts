export const environment = {
  production: true,
  apiUrl: 'https://desa.ies-webcontent.com.mx/xccm/user',
  countryUrl:
    'https://www.placesapi.dev/api/v1/countries?filter%5BareaLt%5D=100000&page%5Bnumber%5D=1&page%5Bsize%5D=10',
  rolAllowed: 'DISTRIBUIDOR',
};
