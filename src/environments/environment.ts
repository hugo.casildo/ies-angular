// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://desa.ies-webcontent.com.mx/xccm/user',
  countryUrl:
    'https://www.placesapi.dev/api/v1/countries?filter%5BareaLt%5D=100000&page%5Bnumber%5D=1&page%5Bsize%5D=10',
  rolAllowed: 'DISTRIBUIDOR',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
