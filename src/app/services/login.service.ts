import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Resultado } from '../model/resultado.model';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  
  validateUser(user: User) {
    return this.http.post<Resultado>(`${environment.apiUrl}/validarUsuario`, user);
  }

}
