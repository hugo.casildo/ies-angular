export interface Resultado {
  resultado: Body;
}

export interface Body {
  exito: boolean;
  id_rol: number;
  desc_rol: string;
}