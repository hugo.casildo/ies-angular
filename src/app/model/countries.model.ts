export interface Countries {
  data: Countrie[];
}

export interface Countrie {
  area: number;
  name: string;
  phoneCode: string;
  population: string;
}
