import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,
    private loginService: LoginService
  ) {}

  loginForm = this.fb.group({
    usuario: ['', [Validators.required, Validators.minLength(1)]],
    contrasena: ['', [Validators.required, Validators.minLength(1)]],
  });

  onSubmit() {
    this.loginService.validateUser(this.loginForm.value)
      .subscribe(({resultado}) => {
      if (resultado?.desc_rol === environment.rolAllowed) {
        sessionStorage.setItem('rol', resultado?.desc_rol);
        this.router.navigate(['/loged/dashboard']);
      } else if (resultado && resultado.desc_rol !== environment.rolAllowed) {
        this.showToaster('Usuario no permitido');
      } else {
        this.showToaster('Error en usuario o contraseña');
      }
    });
  }

  showToaster(message: string) {
    this.snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }
}
