import { Component } from '@angular/core';

const VALUE = 'value';
const NAME = 'name';

@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css'],
})
export class ComponenteUnoComponent {
  constructor() {}
  array = [
    { value: 1, name: 'CampoUno' },
    { value: 2, name: 'CampoDos' },
    { value: 3, name: 'CampoTres' },
    { value: 4, name: 'CampoCuatro' },
    { value: 5, name: 'CampoCinco' },
    { value: 6, name: 'CampoSeis' },
  ];

  secondArray = [
    { value: 21, name: 'a' },
    { value: 20, name: 'b' },
    { value: 19, name: 'c' },
    { value: 18, name: 'd' },
    { value: 17, name: 'e' },
    { value: 16, name: 'f' },
  ];

  obj = {
    CampoUno: 1,
    CampoDos: 2,
    CampoTres: 3,
    CampoCuatro: 4,
    CampoCinco: 5,
    CampoSeis: 6,
  };

  toString(element: any) {
    return JSON.stringify(element);
  }

  resultados(array: any) {
    return array.map((item: object) => {
      let element = {} as any;
      element[Object.values(item)[1]] = Object.values(item)[0];
      return element;
    });
  }
  
  inverse(obj: Object) {
    return Object.entries(obj).map(item => {
      let element = {} as any;
      element[VALUE] = Object.values(item)[1];
      element[NAME] = Object.values(item)[0];
      return element;
    })
  }
}
