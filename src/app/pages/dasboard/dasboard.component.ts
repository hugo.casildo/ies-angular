import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Countrie } from 'src/app/model/countries.model';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.component.html',
  styleUrls: ['./dasboard.component.css'],
})
export class DasboardComponent implements OnInit {
  countries = [] as Countrie[];
  constructor(
    private fb: FormBuilder,
    private countryService: CountryService
  ) {}

  ngOnInit(): void {
    this.countryService.getCountries().subscribe(({ data }) => {
      this.countries = data;
    });
  }

  dashboardForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(1)]],
    email: ['', [Validators.required]],
    date: ['', [Validators.required, Validators.minLength(1)]],
    country: [''],
  });

  myFilter(d: Date | null): boolean {
    const date = d || new Date();
    const currentDate = new Date();
    const maxDate = new Date();
    const minDate = new Date(currentDate.setMonth(currentDate.getMonth() - 11));
    maxDate.setDate(currentDate.getDate() + 1);
    return date > minDate && date < maxDate;
  }

  onSubmit() {
    alert(JSON.stringify(this.dashboardForm.value));
  }
}
