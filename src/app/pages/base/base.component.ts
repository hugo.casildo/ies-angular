import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css'],
})
export class BaseComponent {
  constructor(private router: Router) {}

  public options = [
    { id: 0, name: 'Dashboard', path: '/loged/dashboard' },
    { id: 0, name: 'Primer elemento', path: '/loged/componente' },
  ];
  public redirect(path: string) {
    this.router.navigate([path]);
  }

  logOut() {
    sessionStorage.setItem('rol', '')
    this.redirect('/')
  }
}
