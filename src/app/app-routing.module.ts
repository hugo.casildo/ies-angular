import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './admin.guard';
import { BaseComponent } from './pages/base/base.component';
import { ComponenteUnoComponent } from './pages/componente-uno/componente-uno.component';
import { DasboardComponent } from './pages/dasboard/dasboard.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'loged',
    component: BaseComponent,
    canActivate: [AdminGuard],
    children:[
      {
        path: 'dashboard',
        component: DasboardComponent
      },
      {
        path: 'componente',
        component: ComponenteUnoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
